<?php

/**
 * This is the model class for table "screentyper_script".
 *
 * The followings are the available columns in table 'screentyper_script':
 * @property integer $id
 * @property string $script_key
 * @property integer $user_id
 * @property string $content
 * @property integer $access
 * @property string $create_time
 * @property string $update_time
 */
class Script extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Script the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'screentyper_script';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('script_key, create_time', 'required'),
			array('user_id, access', 'numerical', 'integerOnly'=>true),
			array('script_key', 'length', 'max'=>20),
			array('content, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, script_key, user_id, content, access, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'script_key' => 'Script Key',
			'user_id' => 'User',
			'content' => 'Content',
			'access' => 'Access',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('script_key',$this->script_key,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('access',$this->access);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}