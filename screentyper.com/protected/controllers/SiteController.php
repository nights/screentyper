<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionLoadScript()
	{


		if(isset($_POST['script_key']))
		{
		
			$criteria = new CDbCriteria;
			$criteria->addCondition('script_key = :script_key');
			$criteria->params[':script_key'] = $_POST['script_key'];
			//$criteria->order = 'create_time DESC';
			//$criteria->limit = 20;
					
			$script = Script::model()->find( $criteria );
			if( !isset( $script ) )
			{
				$criteria = new CDbCriteria;
				$criteria->addCondition('script_key = :script_key');
				$criteria->params[':script_key'] = 'empty';
				$script = Script::model()->find( $criteria );
			}

			$data = array( 
				'script_content' => $script->content,
			);
			echo json_encode($data);
			return $data;

		}
	}

	public function actionSaveScript()
	{

		if(isset($_POST['script_key']))
		{
			$criteria = new CDbCriteria;
			$criteria->addCondition('script_key = :script_key');
			$criteria->params[':script_key'] = $_POST['script_key'];
			$script = Script::model()->find( $criteria );

			if( !isset( $script ) )
			{
				$script = new Script;
				$script->script_key = $_POST['script_key'];
				$script->create_time = date( "Y-m-d H:i:s" );
			}
			else			
				$script->update_time = date( "Y-m-d H:i:s" );

			//$message->user_id = Yii::app()->user->getId();
			$script->content = $_POST['content'];
			//$message->harbour_id = HarbourController::getCurrent();

			//if(isset($_POST['recipient_id']))
			//{
			//	$recipient_id = htmlspecialchars( $_POST['recipient_id'] );
			//	$message->recipient_id = $recipient_id;
			//}

			if( $script->validate() )
			{
				$script->save( false );
				echo 'script saved';
			}
			else
				echo CHtml::errorSummary( $script );
		}

		return;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}