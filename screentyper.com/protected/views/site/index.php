
<?php 

if( isset( $_GET['key'] ) )
{
	$script_key = $_GET['key']; 
}
else
{
	$script_key = substr( uniqid(), 0, 6 );
	?>

	<script type="text/javascript">
	$(document).ready(function () 
	{
		//if(window.location.href.indexOf("www-dev") > -1) 
		{
			//var bar = window.location.href;
			//var foo = bar.replace("www-dev","www");
			//foo = foo.replace("http","https");
			window.location.href = "<?php echo Yii::app()->createUrl( 'site/index', array('key' => $script_key) ); ?>";
		}
	});
	</script>

<?php
}

?>

<div id="script_key" style="display:none"><?php echo $script_key; ?></div>
<div class="script_url">
	URL: 
	<span style="">http://www.screentyper.com/?key=<?php echo $script_key; ?>
	<?php //echo Yii::app()->createUrl( 'site/index', array('key' => $script_key) ); ?>	
	</span>
</div>

<style>

.script_url
{
	width: 400px;

	color: #fff;
	font-family: verdana,helvetica,arial,sans-serif;

	margin-bottom: 5px;
	text-align: left;
	font-size: 14px;
	font-weight: normal;
	line-height: 22px;
	
opacity: 0.7;
	background-color: #000;
	border: solid 1px #bbb;
	
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;

	padding-left: 10px;
	margin-bottom: 15px;

	-moz-box-shadow: 0px 0px 3px 1px #bbb;
	-webkit-box-shadow: 0px 0px 3px 1px #bbb;
	box-shadow: 0px 0px 3px 1px #bbb;

}	

#skinbar
{
	float: right;
	opacity: 0.8;
	position: relative;
	z-index: 1;
	
	display: inline-block;
	width: 80px;

	background-color: #000;
	border: solid 1px #bbb;
	
	margin-bottom: 10px;
	padding: 5px;

	color: #fff;
	font-size: 14px;
	font-weight: bold;
	text-align: center;

	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
#skindropdown
{
	position: absolute;
	z-index: 1;
	display: none;
	background-color: #000;
	border: solid 1px #bbb;

	margin-top: 5px;

	padding: 5px;

	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}
#statusbar
{
	margin-left: 1px;
}
.editor_setstyle
{
	cursor: pointer;
	display: block;
	width: 100px;

	background-color: #fff;
	margin-top: 10px;
	padding: 5px;

	color: #46767e;
	font-size: 14px;
	font-weight: bold;
	text-align: center;

	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;

	-moz-box-shadow: 0px 0px 3px 1px #275259;
	-webkit-box-shadow: 0px 0px 3px 1px #275259;
	box-shadow: 0px 0px 3px 1px #275259;
}


</style>

<div id="skinbar" >
	THEME
    <div id="skindropdown">

		<?php
		if ($handle = opendir( './skins') ) {
			$blacklist = array('.', '..', 'somedir', 'somefile.php');
			while (false !== ($skin = readdir($handle))) {
				if (!in_array($skin, $blacklist)) {
					echo '<div editorstyle="'.$skin.'" class="'.editor_setstyle.'">'.strtoupper($skin).'</div>'."\n";
					
					//echo '<li editorstyle="'.$skin.'" class="editor_setstyle"><a href="#">'.strtoupper($skin).'</a></li>'."\n";
				}
			} 
			closedir($handle);
		}

	//<div editorstyle="default" class="editor_setstyle">DEFAULT</div>
	//<div editorstyle="blackhole" class="editor_setstyle">BLACKHOLE</div>

	?>
	</div>
</div>

<div id="statusbar">
	<div class="format_element" id="format_slugline">SLUGLINE</div>
	<div class="format_element" id="format_action">ACTION</div>
	<div class="format_element" id="format_character">CHARACTER</div>
	<div class="format_element" id="format_dialog">DIALOG</div>
	<div class="format_element" id="format_transition">TRANSITION</div>
</div>


<style>

	<?php $editorscale = 4.16; ?>
	<?php $inchmm = 25.4; ?>

	#editorcontainer
	{
		width: <?php echo 8.5*$editorscale*$inchmm; ?>px;

		min-height: <?php echo 11.0*$editorscale*$inchmm; ?>px;
		height:auto !important;
		height: <?php echo 11.0*$editorscale*$inchmm; ?>px;

		/*
		font: medium -moz-fixed;
		font: -webkit-small-control;*/

		font-size: 18px;
		font-family: courier,	 /* Unix+X, MacOS */ monospace;
		line-height: 18px;
	}


	#editorpanel
	{
		padding-left: <?php echo 0.0*$editorscale*$inchmm; ?>px;
		padding-right: <?php echo 0.0*$editorscale*$inchmm; ?>px;
		padding-top: <?php echo 0.7*$editorscale*$inchmm; ?>px;
		padding-bottom: <?php echo 0.7*$editorscale*$inchmm; ?>px;
	}

	#editorarea
	{
		/*
		border: 1px solid gray;
		box-shadow: 1px 1px 1px 0 lightgray inset;  
		*/
		/*
		-moz-appearance: textfield-multiline;
		-webkit-appearance: textarea;
		*/
		white-space: normal;

		resize: none;

		background-color: red;
		background-color: Transparent;
	}

	.decoration_underline
	{
		text-dectoration: underline;
	}

	.editor_pagenumber
	{
		text-align: right;
		text-transform:uppercase;

		padding-left: <?php echo round(1.5*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(1.0*$editorscale*$inchmm); ?>px;
	}

	.editor_slugline
	{
		text-align: left;
		text-transform:uppercase;

		padding-left: <?php echo round(1.5*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(1.0*$editorscale*$inchmm); ?>px;
	}

	.editor_action
	{
		text-align: left;
		/* text-transform:capitalize; */

		padding-left: <?php echo round(1.5*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(1.0*$editorscale*$inchmm); ?>px;
	}

	.editor_character
	{
		text-align: left;
		text-transform:uppercase;

		padding-left: <?php echo round(4.1*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(1.1*$editorscale*$inchmm); ?>px;
	}
	
	.editor_dialog
	{
		text-align: left;

		padding-left: <?php echo round(2.9*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(2.3*$editorscale*$inchmm); ?>px;
	}

	.editor_transition
	{
		text-align: right;

		padding-left: <?php echo round(6.0*$editorscale*$inchmm); ?>px;
		padding-right: <?php echo round(1.0*$editorscale*$inchmm); ?>px;
	}

	.coloring
	{
		background-color: #777;
	}

	.formatting
	{
		line-spacing: 18px;
		margin-bottom: 18px;
	}

	.col
	{
		position: relative;
		width: <?php echo 8.5*$editorscale*$inchmm; ?>px;

		min-height: <?php echo 11.0*$editorscale*$inchmm; ?>px;
		height:auto !important;
		height: <?php echo 11.0*$editorscale*$inchmm; ?>px;

		border:1px solid #999;
		background-color: #ccc;
		overflow:auto;

		margin-top: 20px;

		font-size: 18px;
		font-family: courier,	 /* Unix+X, MacOS */ monospace;
		line-height: 18px;

		padding-left: <?php echo 0.0*$editorscale*$inchmm; ?>px;
		padding-right: <?php echo 0.0*$editorscale*$inchmm; ?>px;
		padding-top: <?php echo 0.7*$editorscale*$inchmm; ?>px;
		padding-bottom: <?php echo 0.7*$editorscale*$inchmm; ?>px;

	}

	.editorpage
	{
		opacity: 0.7;
		color: #fff;
		background-color: #000 !important;
	}


</style>

<div id="editorcontainer" class="">

	<div id="editorpanel" style="">

		<div class="formatting editor_pagenumber">7.</div>

		<div id="editorarea" class="" contentEditable onblur="onDivBlur();" onmousedown="return cancelEvent(event);" onclick="return cancelEvent(event);" onmouseup="saveSelection();" onkeyup="saveSelection();" onfocus="restoreSelection();">		


<?php
/*
<div class="formatting editor_slugline">INT. BANK VAULT - NIGHT</div>
<div class="formatting editor_action">beatrice picks her way through the ransacked room. Cue sticks, books, papers - everything has been searched. She stoops to pick up a photo of a young boy.</div>
<div class="formatting editor_character">bobby</div>
<div class="formatting editor_dialog">Dammit! I told her not to call me!</div>
<div class="formatting editor_transition">CUT TO:</div>
*/
?>
		</div>

	</div>

</div>

<div id="editorpages" class="editorpage">
</div>


<script>

	var savedRange,isInFocus;
	function saveSelection()
	{
		if(window.getSelection)//non IE Browsers
		{
			savedRange = window.getSelection().getRangeAt(0);
		}
		else if(document.selection)//IE
		{ 
			savedRange = document.selection.createRange();  
		}
	}

	function restoreSelection()
	{
		isInFocus = true;
		document.getElementById("editorarea").focus();
		if (savedRange != null) {
			if (window.getSelection)//non IE and there is already a selection
			{
				var s = window.getSelection();
				if (s.rangeCount > 0) 
					s.removeAllRanges();
				s.addRange(savedRange);
			}
			else 
				if (document.createRange)//non IE and no selection
				{
					window.getSelection().addRange(savedRange);
				}
				else 
				{
					if (document.selection)//IE
					{
						savedRange.select();
					}
				}
		}
	}

	function cancelEvent(e)
	{
		if (isInFocus == false && savedRange != null) {
			if (e && e.preventDefault) {
				//alert("FF");
				e.stopPropagation(); // DOM style (return false doesn't always work in FF)
				e.preventDefault();
			}
			else {
				window.event.cancelBubble = true;//IE stopPropagation
			}
			restoreSelection();
			return false; // false = IE style
		}
	}

	//this part onwards is only needed if you want to restore selection onclick
	var isInFocus = false;
	function onDivBlur()
	{
		isInFocus = false;
	}

	function loadScript()
	{

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: '<?php echo Yii::app()->createUrl( 'site/loadScript' ); ?>',				 
			update: '#result_message',
			data: 'script_key=' + jQuery('#script_key').html() +'&isAjaxRequest=1',
			success: function( data )
			{
				jQuery('#editorarea').html( data.script_content );
				//showCargo();
			}
		});
		return true;
	}

	function saveScript()
	{
		if( jQuery('#editorarea').html() == "" )
			return false;

		$.ajax({
			type: 'POST',
			url: '<?php echo Yii::app()->createUrl( 'site/saveScript' ); ?>',				 
			update: '#result_message',
			data: 'script_key=' + jQuery('#script_key').html() + '&content=' + jQuery('#editorarea').html() +'&isAjaxRequest=1',
			success: function( html )
			{
				//jQuery('#transact_status').html( html);
				//showCargo();
			}
		});

		return true;
	}

	$(document).mouseup(function (e)
	{
		var container = $('[contentEditable]');

		if (container.has(e.target).length === 0)
		{
			restoreSelection();
		}

	});

	//page layout code
	$.fn.hasOverflow = function()
	{
		var div= document.getElementById($(this).attr('id')); 
		return div.scrollHeight > div.clientHeight;
	};
	$.fn.willOverflow = function( text ) 
	{
		var oldText = $(this).html();
		$(this).append( text );
		var div = document.getElementById($(this).attr('id')); 
		var overflowStatus = div.scrollHeight > div.clientHeight;
		$(this).html( oldText );
		return overflowStatus;
	};
	var pageCounter = 1;
	function pageLayout()
	{
		//var currentCol = $('.col:first');
		var currentEditor = $('#editorarea');
		var text = currentEditor.html();
		//currentCol.html('');
		var wordArray= text.split('</div>');

		if( $('.col').length == 0 )
		{
			$('#editorpages').append('<div id="#editorarea_page' + pageCounter++ + '" class="col scriptstyle editorpage"></div>' );
			//alert( 'new page');
		}
		var currentCol =  $('#editorpages .col:first');
		currentCol.html('');

		for(var x=0; x<wordArray.length; x++)
		{
			var word= wordArray[x];
			if( currentCol.willOverflow( word ) )			
			{
				if( currentCol.next('.col').length == 0 )
				{
					$('#editorpages').append('<div id="#editorarea_page' + pageCounter++ + '" class="col scriptstyle editorpage"></div>' );
					//alert( 'new page');
				}

				currentCol = currentCol.next('.col');
				currentCol.html('');
			}
			else
			{
			}
			currentCol.append(word );
		}
	}

	var lastSaveTimer = null;
	$(document).ready(function() 
	{

		$('body').addClass('pagestyle');
		$('#editorcontainer').addClass('editorstyle');

		pageLayout();
		loadScript();

		$('#skinbar').click(function() 
		{
			$('#skindropdown').slideDown();
		});

		//$("[contentEditable]").text().replace(/\n/g,"<br />")

		$('[contentEditable]').keyup(function(e) {

			if( lastSaveTimer != null )
				clearTimeout(lastSaveTimer);
			lastSaveTimer = setTimeout("saveScript()",'3000');
		});

		$('[contentEditable]').keydown(function(e) {

			editor_element = window.getSelection().anchorNode.parentNode;

			//if (e.keyCode == 8 || //backspace
			//	e.keyCode == 13 || //enter
			//	e.keyCode == 46 //delete ) 

			setTimeout("pageLayout()", '50');
			{
				//setInterval(function(){myTimer()},100);
				
				//pageLayout();
			}

			if( e.keyCode == 8 || 
				e.keyCode == 13 || 
				e.keyCode == 46 ) 
			{
				// insert 2 br tags (if only one br tag is inserted the cursor won't go to the second line)
				//document.execCommand('insertHTML', false, '<br><br>');
				$('#editorarea :not(div)').contents().unwrap();
				
				//var b = document.getElementById('editorarea');

				//while(b.length) {
				//	var parent = b[ 0 ].parentNode;
				//	while( b[ 0 ].firstChild ) {
				//		parent.insertBefore(  b[ 0 ].firstChild, b[ 0 ] );
				//	}
				//	parent.removeChild( b[ 0 ] );
				//}

				//if( $(editor_element).attr("id")!='editorarea' &&
				//	$(editor_element).parent().attr("id") != 'editorarea' )
				//{
				//	$(editor_element).contents().unwrap();
				//	editor_element = window.getSelection().anchorNode.parentNode;
				//	//alert();
				//}
				
				// prevent the default behaviour of return key pressed
				//return false;
			}

			$(editor_element).trigger('click');

		});


		//$('#logo').click(function() {

		//	$('[contentEditable]').focus();
		//});


		$('#page').on('click', '.formatting', function(event)		
		{

			var editor_formatting = $(this).attr("class");
			var formatting_id = editor_formatting.replace("formatting editor_","format_");

			$('.format_element').removeClass('format_active');
			$('#' + formatting_id).addClass('format_active');

		});

		$('#page').on('click', '.format_element', function(event)	
		{
			
			var formatting_id = $(this).attr("id");
			var format_class = formatting_id.replace("format_","editor_");

			editor_element = window.getSelection().anchorNode.parentNode;

			$(editor_element).removeClass();
			$(editor_element).addClass('formatting');
			//$(editor_element).addClass('coloring');
			$(editor_element).addClass(format_class);
			
			$('.format_element').removeClass('format_active');
			$('#' + formatting_id).addClass('format_active');
		});

		$('#page').on('click', '.editor_setstyle', function(event)	
		{

			var editorstyle = $(this).attr("editorstyle");

			$.ajax(
			{
				url: "./skins/" + editorstyle + "/" + editorstyle + ".css",
				success:function(data){
					 $("<style></style>").appendTo("head").html(data);
					$('#skindropdown').fadeOut('fast');
				}
			})
			//
			//$('#editorcontainer').removeClass();
			//$('#editorcontainer').addClass('editorstyle_' + editorstyle);

			//$('body').removeClass();
			//$('body').addClass('pagestyle_' + editorstyle);
		});

		//$('[contentEditable]').keydown(function(e) {
		//	// trap the return key being pressed
		//	if (e.keyCode == 13) {
		//	  // insert 2 br tags (if only one br tag is inserted the cursor won't go to the second line)
		//	  document.execCommand('insertHTML', false, '<br><br>');
		//	  // prevent the default behaviour of return key pressed
		//	  return false;
		//	}
		//});

	});



</script>