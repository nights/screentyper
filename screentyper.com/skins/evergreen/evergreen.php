
/* STYLE: BLACKHOLE */

.format_element
{
	opacity: 0.9;
	
	position: relative;
	z-index: 1;
	
	display: inline-block;
	width: 120px;

	background-color: #004118;
	border: solid 1px #004118;
	
	margin-bottom: 3px;
	padding: 5px;

	color: #fff;
	font-size: 14px;
	font-weight: bold;
	text-align: center;

	-webkit-border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px 5px 5px 5px;
	border-radius: 5px 5px 5px 5px;

	-moz-box-shadow: 0px 0px 3px 1px #00bf48;
	-webkit-box-shadow: 0px 0px 3px 1px #00bf48;
	box-shadow: 0px 0px 3px 1px #00bf48;
}

.format_active
{
	color: #fff;
	background-color: #00bf48 !important;
	border: solid 1px #016e15;
	
	-moz-box-shadow: 0px 0px 6px 2px #adffbc;
	-webkit-box-shadow: 0px 0px 6px 2px #adffbc;
	box-shadow: 0px 0px 6px 2px #adffbc;

	text-shadow:
	-1px -1px 0 #666,
	1px -1px 0 #666,
	-1px 1px 0 #666,
	1px 1px 0 #666; 
	
	position: relative;
	top: -2px;
}

body.pagestyle
{
	background-color: #041215;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#041215), to(#000));
	background: -webkit-linear-gradient(#041215, #000);
	background: -moz-linear-gradient(#041215, #000);
	background: -ms-linear-gradient(#041215, #000);
	background: -o-linear-gradient(#041215, #000);
	background: linear-gradient(#041215, #000);
	
	background: #000 url('<?php echo Yii::app()->params->urlBase; ?>/skins/evergreen/bg.jpg') no-repeat center center fixed; 
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
#editorcontainer.editorstyle
{
	opacity: 0.9;
	
	color: #fff;
	border: solid 1px #004118;

	-webkit-border-radius: 8px 8px 8px 8px;
	-moz-border-radius: 8px 8px 8px 8px;
	border-radius: 8px 8px 8px 8px;
	
	-moz-box-shadow: 0px 0px 25px 5px #00bf48;
	-webkit-box-shadow: 0px 0px 25px 5px #00bf48;
	box-shadow: 0px 0px 25px 5px #00bf48;
	
	background-color: #081401;
	background: -webkit-gradient(linear, 0 0, 0 bottom, from(#081401), to(#111));
	background: -webkit-linear-gradient(#081401, #111);
	background: -moz-linear-gradient(#081401, #111);
	background: -ms-linear-gradient(#081401, #111);
	background: -o-linear-gradient(#081401, #111);
	background: linear-gradient(#081401, #111);  
	
	
}